import { View, Text } from 'react-native'
import React from 'react'
import MapView, { PROVIDER_GOOGLE }  from 'react-native-maps';
const GoogleMapsCode = () => {
    // Enable 
// Maps SDK for Android in Google cloud Platform

// fsq3FkuAb17s5xyPeWULWtcMfTcOEK8RWFqFhSK3gQhf9PI=
  return (
    <View style={{flex:1}}>
     <MapView
     style={{
        width:"100%",
        height:500
     }}
    initialRegion={{
      latitude: 37.78825,
      longitude: -122.4324,
      latitudeDelta: 0.0922,
      longitudeDelta: 0.0421,
    }}
  />
  </View>

  )
}

export default GoogleMapsCode