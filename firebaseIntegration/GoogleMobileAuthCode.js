import { View, Text ,Button,TextInput} from 'react-native'
import React,{useState} from 'react'
import auth from '@react-native-firebase/auth';
const GoogleMobileAuthCode = () => {
  // If null, no SMS has been sent
  const [confirm, setConfirm] = useState(null);

  const [code, setCode] = useState('');

  // Handle the button press
  async function signInWithPhoneNumber(phoneNumber) {
    const confirmation = await auth().signInWithPhoneNumber(phoneNumber);
    console.log(confirmation)
    setConfirm(confirmation);
  }

  async function confirmCode() {
    try {
      const success = await confirm.confirm(code);
      console.log(success,"success")
    } catch (error) {
      console.log('Invalid code.');
    }
  }

  if (!confirm) {
    return (
      <Button
        title="Phone Number Sign In"
        onPress={() => signInWithPhoneNumber('+91 7871921240')}
      />
    );
  }

  return (
    <>
      <TextInput value={code} onChangeText={text => setCode(text)} />
      <Button title="Confirm Code" onPress={() => confirmCode()} />
    </>
  );
}

export default GoogleMobileAuthCode