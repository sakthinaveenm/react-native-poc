import React, { useState, useEffect } from 'react';
import { View, Text ,Button  } from 'react-native';
import auth from '@react-native-firebase/auth';
import { GoogleSignin ,statusCodes } from '@react-native-google-signin/google-signin';
function GoogleSignInCode() {

  async function onGoogleButtonPress() {
    // Get the users ID token
    const { idToken } = await GoogleSignin.signIn();
    console.log(idToken)
    // Create a Google credential with the token
    const googleCredential = auth.GoogleAuthProvider.credential(idToken);
  
    // Sign-in the user with the credential
    return auth().signInWithCredential(googleCredential);
  }
  // Set an initializing state whilst Firebase connects
  const [initializing, setInitializing] = useState(true);
  const [signed, isSigned] = useState(false);

  const [user, setUser] = useState();

  // Handle user state changes
  function onAuthStateChanged(user) {
    console.log(user,"USER")
    setUser(user);
    if (initializing) setInitializing(false);
  }
  const signOut = async () => {
    try {
       const signout  = await GoogleSignin.signOut();
      setUser(null); // Remember to remove the user from your app's state as well
      console.log(signout,"signout")
    } catch (error) {
      console.error(error);
    }
  };
  
  const isSignedIn = async () => {
    const isSignedIn = await GoogleSignin.isSignedIn();
    isSigned({ isLoginScreenPresented: !isSignedIn });
  };
  const revokeAccess = async () => {
    try {
      await GoogleSignin.revokeAccess();
      // Google Account disconnected from your app.
      // Perform clean-up actions, such as deleting data associated with the disconnected account.
    } catch (error) {
      console.error(error);
    }
  };

  const getCurrentUserInfo = async () => {
    try {
      const userInfo = await GoogleSignin.signInSilently();
      console.log(userInfo,"userInfo4")
      if(userInfo){
        setUser(userInfo.user)
      }
    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_REQUIRED) {
        // user has not signed in yet
        console.log(error.code,"userInfo4")
      } else {
        // some other error
        console.log(error,"userInfo4")

      }
    }
  };
  
  useEffect(() => {
    GoogleSignin.configure({
      webClientId: '805026272170-1g7bg75utl313bb3o6mqkl09b2s5ilrq.apps.googleusercontent.com',
      
    });
    isSignedIn();
    getCurrentUserInfo();
    // const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
    // return subscriber; // unsubscribe on unmount
  }, []);



  if (!user) {
    return (
      <View>
        <Text>Login</Text>
        <Button
      title="Google Sign-In"
      onPress={() => onGoogleButtonPress().then((res) =>{ 
        console.log('Signed in with Google!',res.user.email)
        onAuthStateChanged(res.user)
      }
        )
        // Output
    // {"additionalUserInfo": {"isNewUser": true, "profile": {"aud": "805026272170-1g7bg75utl313bb3o6mqkl09b2s5ilrq.apps.googleusercontent.com", "azp": "805026272170-10a16fakm8odepadv699fptmgji82dut.apps.googleusercontent.com", "email": "sakthi.kpost@gmail.com", "email_verified": true, "exp": 1658904135, "family_name": "Naveen", "given_name": "Sakthi", "iat": 1658900535, "iss": "https://accounts.google.com", "locale": "en-GB", "name": "Sakthi Naveen", "picture": "https://lh3.googleusercontent.com/a-/AFdZucqs5rbAMmc-Z_BFPO8MD039PH-68X6211E8viv_=s96-c", "sub": "101876619161366409197"}, "providerId": "google.com"}, "user": {"displayName": "Sakthi Naveen", "email": "sakthi.kpost@gmail.com", "emailVerified": true, "isAnonymous": false, "metadata": [Object], "phoneNumber": null, "photoURL": "https://lh3.googleusercontent.com/a-/AFdZucqs5rbAMmc-Z_BFPO8MD039PH-68X6211E8viv_=s96-c", "providerData": [Array], "providerId": "firebase", "tenantId": null, "uid": "SJRXIwSzyaXPX4SplgLl9Er6Xmn1"}}

      }
    />
      </View>
    );
  }

  return (
    <View>
      <Text>Welcome {user.email}</Text>
      <Button title="Logout" 
        onPress={()=>signOut()}
      />
        <Text>{JSON.stringify(signed)}</Text>
      
    </View>
  );
}

export default GoogleSignInCode;